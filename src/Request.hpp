/*
  Copyright (c) 2020 Jose Manuel Sanchez Madrid.
  This file is licensed under MIT license. See file LICENSE for details.
*/

#ifndef _TETO_REQUEST_HPP
#define _TETO_REQUEST_HPP 1

#include<cpr/cpr.h>
#include "HttpMethod.hpp"

using namespace cpr;

class Request
{
public:
	HttpMethod method;
	Url url;
	Header headers;
	Body body;

	bool operator == (const Request& rho) const
	{
		return ( method == rho.method && url == rho.url && headers == rho.headers && body == rho.body );
	}

};

#endif
