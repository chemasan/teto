/*
  Copyright (c) 2020 Jose Manuel Sanchez Madrid.
  This file is licensed under MIT license. See file LICENSE for details.
*/

#ifndef _TETO_HTTPMETHOD_HPP
#define _TETO_HTTPMETHOD_HPP 1

#include <string>
enum class HttpMethod { GET, POST, PUT, DELETE, HEAD, OPTIONS, PATCH };

HttpMethod parseMethod(const std::string& str);

#endif
