/*
  Copyright (c) 2020 Jose Manuel Sanchez Madrid.
  This file is licensed under MIT license. See file LICENSE for details.
*/

#include "systemfunctions.hpp"
#include <unistd.h>
#include <cstring>

using namespace std;

string hostname()
{
	char buffer[1024];
	bzero(buffer, 1024);
	gethostname(buffer, 1023);
	return string(buffer);
}
int pid()
{
	return getpid();
}
