/*
  Copyright (c) 2020 Jose Manuel Sanchez Madrid.
  This file is licensed under MIT license. See file LICENSE for details.
*/

#ifndef _TETO_RESULTSFUNCTIONS_HPP
#define _TETO_RESULTSFUNCTIONS_HPP 1

#include <vector>
#include "ResultSample.hpp"

using namespace std;

unsigned int countErroredRequests(const vector<ResultSample>& results);
unsigned int countUnsuccessfulRequests(const vector<ResultSample>& results);
vector<float> getSortedLatencies(const vector<ResultSample>& results);
bool compareWithEpsilon(double value1, double value2, double epsilon = 0.00005);
template<typename T> T nearestRankPercentile(double percentile, vector<T> values);
void displaySummary(const vector<ResultSample>& results);
void saveSamples(string filename, const vector<ResultSample>& results);
void saveToInflux(const vector<ResultSample>& results, const string& url, const string& db, const string& measure, const string& user, const string& pass);

#endif
