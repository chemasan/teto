/*
  Copyright (c) 2020 Jose Manuel Sanchez Madrid.
  This file is licensed under MIT license. See file LICENSE for details.
*/

#ifndef _TETO_SESSIONSPOOL_HPP
#define _TETO_SESSIONSPOOL_HPP 1

#include <cpr/cpr.h>
#include <vector>

using namespace std;
using namespace cpr;

class SessionPool
{
public:
	SessionPool(size_t  poolSize);
	Session& getSession();

private:
	vector<Session> sessions;
	size_t index;
};

#endif
