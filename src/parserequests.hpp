/*
  Copyright (c) 2020 Jose Manuel Sanchez Madrid.
  This file is licensed under MIT license. See file LICENSE for details.
*/

#ifndef _TETO_PARSEREQUESTS_HPP
#define _TETO_PARSEREQUESTS_HPP 1

#include <cpr/cpr.h>
#include <string>
#include "Request.hpp"

using namespace std;
using namespace cpr;

pair<string,string> parseHeader(const string& str);
Header parseHeaders(const vector<string>& headersVector);
Request parseRequest(string requestStr);
vector<Request> parseRequests(istream& source, const string& delimiter = ".");
vector<Request> parseRequestsFile(const string& filePath, const string& delimiter = ".");
void completeUrls(vector<Request>& requests, const Url& url);
void setKeepAlive(vector<Request>& requests, bool isKeepAlive);

#endif
