/*
  Copyright (c) 2020 Jose Manuel Sanchez Madrid.
  This file is licensed under MIT license. See file LICENSE for details.
*/

#include "SessionPool.hpp"
#include "RandomGenerator.hpp"
#include "stressfunctions.hpp"

using namespace std;
using namespace cpr;

Session nullSession;

Response doRequest(Request request, long timeout, Session& sessionRef)
{
	Session localSession;
	Session& session = &sessionRef == &nullSession ? localSession : sessionRef ; //If null use local object
	session.SetUrl(request.url);
	session.SetHeader(request.headers);
	session.SetBody(request.body);
	session.SetTimeout(Timeout(timeout));
	switch (request.method)
	{
		case HttpMethod::POST : return session.Post();
		case HttpMethod::PUT :  return session.Put();
		case HttpMethod::DELETE : return session.Delete();
		case HttpMethod::HEAD : return session.Head();
		case HttpMethod::OPTIONS : return session.Options();
		case HttpMethod::PATCH : return session.Patch();
		default: return session.Get();
	}
}
ResultSample runCheck(const Request& request, long timeout, Session& session)
{
	ResultSample result;
	result.sampleTime = chrono::system_clock::now();
	result.startTime = chrono::steady_clock::now();
	auto response = doRequest(request, timeout, session);
	result.endTime = chrono::steady_clock::now();
	auto duration = result.endTime - result.startTime;
	result.responseCode = response.status_code;
	result.responseSize = response.text.size();
	return result;
}
future<ResultSample> runAsyncCheck(const Request& request, long timeout, Session& session)
{
	return async(launch::async, runCheck, request, timeout, ref(session));
}

vector<ResultSample> runTest(const vector<Request>& requests, unsigned int testtimesecconds, unsigned int throwput, unsigned int connections, bool randomized, long timeout)
{
	vector<Request>::size_type index = 0;
	SessionPool sessions(connections);
	RandomGenerator<vector<Request>::size_type> random(0, requests.size()-1);
	vector<future<ResultSample>> resultfutures;
	unsigned int waitmillis = 1000 / throwput;
	for (unsigned int remaining = (testtimesecconds * 1000); remaining > 0; remaining -= waitmillis)
	{
		if (index >= requests.size())  index = 0;
		const Request& request = ( (randomized) ? requests[random.generate()] : requests[index] );
		++index;
		Session& session = connections > 0 ? sessions.getSession() : nullSession;
		resultfutures.push_back(runAsyncCheck(request, timeout, session ));
		this_thread::sleep_for(chrono::milliseconds(waitmillis));
	}
	vector<ResultSample> results;
	for (auto& resultfuture : resultfutures)     results.push_back(resultfuture.get());
	return results;
}
