/*
  Copyright (c) 2020 Jose Manuel Sanchez Madrid.
  This file is licensed under MIT license. See file LICENSE for details.
*/

#include <tclap/CmdLine.h>
#include "Configuration.hpp"
#include "parserequests.hpp"

#ifndef VERSION
#define VERSION "none"
#endif

using namespace std;

Configuration parseConfig(int argc, char** argv)
{
	using namespace TCLAP;
	CmdLine cmd("web load TEsting TOol",' ', VERSION);
	ValueArg<unsigned int> throwput("r", "rate", "Target request rate in requests per seccond. Defaults to 1.", false, DEFAULT_THROWPUT, "rate");
	ValueArg<unsigned int> warmupDurationInSecconds("w", "warmup", "Warmup duration time in seconds. Warmup runs before the test, making requests at the same rate as during the test, but any latency result or metric generated during the warmup is ignored and don't compute for the test results. Defaults to 0 (No warmup).", false, DEFAULT_WARMUP_DURATION_SECCONDS, "warmup");
	ValueArg<unsigned int> testDurationInSecconds("t", "time", "Test duration time in seconds. Defaults to 60.", false, DEFAULT_TEST_DURATION_SECCONDS, "time");
	ValueArg<unsigned int> connections("c", "connections", "Simultaneous HTTP keep-alive connections to use during the tests. Defaults to 0, that means to not use keep-alive and create a new connection for each request.", false, DEFAULT_CONNECTIONS, "connections");
	ValueArg<long> timeoutInMillis("","timeout", "Timeout for the full request to take place. Defaults to 0, that means no timeout.",false,DEFAULT_TIMEOUT_MILLIS, "timeout");
	ValueArg<string> outputSamplesFile("o", "output", "File path to save result samples. By default result samples are not saved.", false, "", "output");
	ValueArg<string> requestsFile("f", "file", "Use the requests in the given file during the test. By default, the requests are forged based on the given options.", false, "", "file");
	ValueArg<string> delimiter("","delimiter", "Delimiter to separate requests in the requests file. Defaults to '.'",false,".","delimiter");
	SwitchArg random("R","random", "Randomize usage of requests from requests file. By default requests are used in the same order as they appear in the file.",false);
	ValueArg<string> method("X", "method", "HTTP method to use in requests. Defaults to GET.", false, "get", "method");
	ValueArg<string> data("d", "data", "Data to send in the request body for POST and PUT requests. Defaults to an empty string.", false, "", "data");
	MultiArg<string> headers("H", "header", "Additional HTTP Header to include in the requests.", false, "header");
	ValueArg<string> influxUrl("","influx-url","InfluxDB server URL to save result samples into. Not enabled by default.",false,"","url");
	ValueArg<string> influxDatabase("","influx-db","InfluxDB database to save result samples. Teto will try to create it if not exists. Defaults to 'tetoStressDb' if not explicitly set.",false,"tetoStressDb","database");
	ValueArg<string> influxMeasure("","influx-measure","InfluxDB measurement to save result samples into. Defaults to 'tetoStressData' if not explicitly set.",false,"tetoStressData","measurement");
	ValueArg<string> influxUser("","influx-user","InfluxDB username to authenticate into the server. Authentication will not be used if a username is not explicitly set.",false,"","username");
	ValueArg<string> influxPass("","influx-pass","InfluxDB username to authenticate into the server. Defaults to an empty string.",false,"","password");
	UnlabeledValueArg<string> url("url", "Target URL to test.", true, "", "url");

	// adding order determines the order in the help message
	cmd.add(influxPass);
	cmd.add(influxUser);
	cmd.add(influxMeasure);
	cmd.add(influxDatabase);
	cmd.add(influxUrl);
	cmd.add(outputSamplesFile);
	cmd.add(delimiter);
	cmd.add(requestsFile);
	cmd.add(random);
	cmd.add(data);
	cmd.add(headers);
	cmd.add(method);
	cmd.add(timeoutInMillis);
	cmd.add(connections);
	cmd.add(warmupDurationInSecconds);
	cmd.add(testDurationInSecconds);
	cmd.add(throwput);
	cmd.add(url);
	cmd.parse(argc, argv);

	Configuration config;
	config.throwput = throwput.getValue();
	config.connections = connections.getValue();
	config.timeoutInMillis = timeoutInMillis.getValue();
	config.testDurationInSecconds = testDurationInSecconds.getValue();
	config.warmupDurationInSecconds = warmupDurationInSecconds.getValue();
	config.url = Url(url.getValue());
	config.method = parseMethod(method.getValue());
	config.data = Body(data.getValue());
	config.headers = parseHeaders(headers.getValue());
	config.outputSamplesFile = outputSamplesFile.getValue();
	config.requestsFile = requestsFile.getValue();
	config.delimiter = delimiter.getValue();
	config.random = random.getValue();
	config.influxUrl = influxUrl.getValue();
	config.influxDatabase = influxDatabase.getValue();
	config.influxMeasure = influxMeasure.getValue();
	config.influxUser = influxUser.getValue();
	config.influxPass = influxPass.getValue();
	return config;
}

