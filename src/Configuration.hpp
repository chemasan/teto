/*
  Copyright (c) 2020 Jose Manuel Sanchez Madrid.
  This file is licensed under MIT license. See file LICENSE for details.
*/

#ifndef _TETO_CONFIGURATION_HPP
#define _TETO_CONFIGURATION_HPP 1

#include<string>
#include<cpr/cpr.h>
#include "HttpMethod.hpp"

using namespace cpr;
using namespace std;

const unsigned int DEFAULT_TEST_DURATION_SECCONDS = 60;
const unsigned int DEFAULT_WARMUP_DURATION_SECCONDS = 0;
const unsigned int DEFAULT_THROWPUT = 1;
const unsigned int DEFAULT_CONNECTIONS = 0;
const long DEFAULT_TIMEOUT_MILLIS = 0;

class Configuration
{
public:
	Url url;
	HttpMethod method = HttpMethod::GET;
	Header headers;
	Body data = Body("");
	unsigned int testDurationInSecconds = DEFAULT_TEST_DURATION_SECCONDS;
	unsigned int warmupDurationInSecconds = DEFAULT_WARMUP_DURATION_SECCONDS;
	unsigned int throwput = DEFAULT_THROWPUT;
	unsigned int connections = DEFAULT_CONNECTIONS;
	long timeoutInMillis = DEFAULT_TIMEOUT_MILLIS;
	string outputSamplesFile;
	string requestsFile;
	string delimiter = ".";
	bool random = false;
	string influxUrl;
	string influxDatabase;
	string influxMeasure;
	string influxUser;
	string influxPass;
};

Configuration parseConfig(int argc, char** argv);

#endif
