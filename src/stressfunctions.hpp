/*
  Copyright (c) 2020 Jose Manuel Sanchez Madrid.
  This file is licensed under MIT license. See file LICENSE for details.
*/

#ifndef _TETO_STRESSFUNCTIONS_HPP
#define _TETO_STRESSFUNCTIONS_HPP

#include <cpr/cpr.h>
#include <vector>
#include <future>
#include "Request.hpp"
#include "ResultSample.hpp"

using namespace cpr;
using namespace std;

extern Session nullSession; //Trick to emulate NULL with references

Response doRequest(Request request, long timeout = 0, Session& sessionRef = nullSession);
ResultSample runCheck(const Request& request, long timeout = 0, Session& session = nullSession);
future<ResultSample> runAsyncCheck(const Request& request, long timeout = 0, Session& session = nullSession);
vector<ResultSample> runTest(const vector<Request>& requests, unsigned int testtimesecconds = 10, unsigned int throwput = 1, unsigned int connections = 0, bool randomized = false, long timeout = 0);

#endif
