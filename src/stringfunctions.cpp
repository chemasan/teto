/*
  Copyright (c) 2020 Jose Manuel Sanchez Madrid.
  This file is licensed under MIT license. See file LICENSE for details.
*/

#include <algorithm>
#include "stringfunctions.hpp"

using namespace std;

string toLower(string str)
{
    transform(str.begin(), str.end(), str.begin(), ::tolower);
    return str;
}

string ltrim(const string& orig, const string& trimchars)
{
	size_t pos = orig.find_first_not_of(trimchars);
	if (pos == string::npos) return ""s;
	return orig.substr(pos,orig.size()-pos);
}

string rtrim(const string& orig, const string& trimchars)
{
	size_t pos = orig.find_last_not_of(trimchars);
	if (pos == string::npos) return ""s;
	return orig.substr(0,pos+1);
}

string trim(const string& orig, const string& trimchars)
{
	return rtrim(ltrim(orig,trimchars),trimchars);
}

pair<string,string> split2(const string& orig, char delimiter)
{
	auto pos = orig.find(delimiter);
	if (pos == string::npos) return pair<string,string>(orig,"");
	pair<string,string> ret;
	ret.first = orig.substr(0, pos);
	if (pos + 1 < orig.size())   ret.second = orig.substr(pos+1,orig.size()-pos-1);
	return ret;
}
