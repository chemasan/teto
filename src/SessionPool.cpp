/*
  Copyright (c) 2020 Jose Manuel Sanchez Madrid.
  This file is licensed under MIT license. See file LICENSE for details.
*/

#include "SessionPool.hpp"

using namespace cpr;

SessionPool::SessionPool(size_t  poolSize) : sessions(poolSize)
{
}

Session& SessionPool::getSession()
{
	if (sessions.empty()) throw runtime_error("Session Pool is empty. Can't return a valid session.");
	if (index >= sessions.size()) index = 0;
	return sessions[index++];
}

