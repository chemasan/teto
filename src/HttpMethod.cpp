/*
  Copyright (c) 2020 Jose Manuel Sanchez Madrid.
  This file is licensed under MIT license. See file LICENSE for details.
*/

#include <stdexcept>
#include <string>
#include "HttpMethod.hpp"
#include "stringfunctions.hpp"

using namespace std;

HttpMethod parseMethod(const string& str)
{
	string lcasestr = toLower(str);
	if (lcasestr == "get") return HttpMethod::GET;
	if (lcasestr == "post") return HttpMethod::POST;
	if (lcasestr == "put") return HttpMethod::PUT;
	if (lcasestr == "head") return HttpMethod::HEAD;
	if (lcasestr == "delete") return HttpMethod::DELETE;
	if (lcasestr == "options") return HttpMethod::OPTIONS;
	if (lcasestr == "patch") return HttpMethod::PATCH;
	throw runtime_error("parseMethod: Unknown HTTP method '"s + str + "'"s);
}
