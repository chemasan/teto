/*
  Copyright (c) 2020 Jose Manuel Sanchez Madrid.
  This file is licensed under MIT license. See file LICENSE for details.
*/

#include <iostream>
#include <fstream>
#include <algorithm>
#include <cmath>
#include <stdexcept>
#include "resultsfunctions.hpp"
#include "systemfunctions.hpp"
#include <influx.hpp>

using namespace std;

unsigned int countErroredRequests(const vector<ResultSample>& results)
{
	unsigned int erroredRequests = 0;
	for (auto sample : results)
	{
		if (sample.responseCode == 0) ++erroredRequests;
	}
	return erroredRequests;
}
unsigned int countUnsuccessfulRequests(const vector<ResultSample>& results)
{
	unsigned int unsuccessfulRequests = 0;
	for (auto sample : results)
	{
		if (sample.responseCode < 200 || sample.responseCode >= 300) ++unsuccessfulRequests;
	}
	return unsuccessfulRequests;
}
vector<float> getSortedLatencies(const vector<ResultSample>& results)
{
	vector<float> latencies;
	for ( auto sample : results)    latencies.push_back(sample.latencyMillis());
	sort(latencies.begin(), latencies.end());
	return latencies;
}
bool compareWithEpsilon(double value1, double value2, double epsilon)
{
	return (fabs(value1 - value2) < epsilon);
}
template<typename T> T nearestRankPercentile(double percentile, vector<T> values)
{
	static_assert( is_arithmetic<T>::value, "nearestRankPercentile(): values argument is not a vector of a numeric type");
	if (percentile <= 0)   throw out_of_range("nearestRankPercentile(): percentile argument is 0 or a negative number");
	if (values.size() < 1)   throw length_error("nearestRankPercentile(): values argument is an empty vector");
	sort (values.begin(), values.end());
	double rank = (percentile / 100) * values.size();
	long int index = round(rank);
	return values[index -1];
}
void displaySummary(const vector<ResultSample>& results)
{
	auto totalRequests = results.size();
	auto erroredRequests = countErroredRequests(results);
	double percentErroredRequests = ((static_cast<double>(erroredRequests) * 100) / static_cast<double>(totalRequests));
	auto unsuccessfulRequests = countUnsuccessfulRequests(results);
	double percentUnsuccessfulRequests = ((static_cast<double>(unsuccessfulRequests) * 100) / static_cast<double>(totalRequests));
	auto latencies = getSortedLatencies(results);
	auto minLatency = latencies[0];
	auto maxLatency = latencies[totalRequests -1];
	auto percentile25th = nearestRankPercentile(25, latencies);
	auto percentile50th = nearestRankPercentile(50, latencies);
	auto percentile75th = nearestRankPercentile(75, latencies);
	auto percentile90th = nearestRankPercentile(90, latencies);
	auto percentile99th = nearestRankPercentile(99, latencies);
	auto percentile999th = nearestRankPercentile(99.9, latencies);

	cout << "Total requests: " << totalRequests << endl;
	cout << "Errored requests (connection errors and timeouts): " << erroredRequests << endl;
	cout << "% Errored requests: " << percentErroredRequests << endl;
	cout << "Unsuccessful requests (errored or responded with non 2XX status code): " << unsuccessfulRequests << endl;
	cout << "% Unsuccessful requests: " << percentUnsuccessfulRequests << endl;
	cout << "Minimum latency in ms: " << minLatency << endl;
	cout << "25th percentile in ms: " << percentile25th << endl;
	cout << "50th percentile in ms: " << percentile50th << endl;
	cout << "75th percentile in ms: " << percentile75th << endl;
	cout << "90th percentile in ms: " << percentile90th << endl;
	cout << "99th percentile in ms: " << percentile99th << endl;
	cout << "99.9th percentile in ms: " << percentile999th << endl;
	cout << "Maximum latency in ms: " << maxLatency << endl;
}
void saveSamples(string filename, const vector<ResultSample>& results)
{
	ofstream outfile(filename);
	outfile << "#timestampInNanosSinceEpoch latencyInNanoseconds responseCode responseSize" << endl;
	for (auto& result : results)
	{
		outfile << result.sampleTime.time_since_epoch().count();
		outfile << " ";
		outfile << result.latencyNanos();
		outfile << " ";
		outfile << result.responseCode;
		outfile << " ";
		outfile << result.responseSize;
		outfile << endl;
	}
}
void saveToInflux(const vector<ResultSample>& results, const string& url, const string& db, const string & measure, const string& user, const string& pass)
{
	using namespace influx;
	string myhostname = hostname();
	string mypid = to_string(pid());
	vector<Record> records;
	for (auto& result : results)
	{
		Record record;
		record.measurement = measure;
		record.timestamp = result.sampleTime.time_since_epoch().count();
		record.tags["testerHostname"] = myhostname;
		record.tags["testerPid"] = mypid;
		record.fields["latencyInNanoseconds"] = static_cast<int64_t>(result.latencyNanos());
		record.fields["responseCode"] = static_cast<int64_t>(result.responseCode);
		record.fields["responseSize"] = static_cast<int64_t>(result.responseSize);
		records.emplace_back(record);
	}
	Influx server(url, user, pass);
	try
	{
		server.createDb(db);
	} catch (runtime_error& e) {} // The given user may have no permissions to create the database
	server.write(db, records);
}
