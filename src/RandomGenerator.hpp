/*
  Copyright (c) 2020 Jose Manuel Sanchez Madrid.
  This file is licensed under MIT license. See file LICENSE for details.
*/

#include <random>
#include <iostream>
#include <type_traits>

using namespace std;

template<typename T>
class RandomGenerator
{
private:
	random_device rd;
	mt19937 mt;
	uniform_int_distribution<T> distribution;
public:
	RandomGenerator(T min, T max) : mt(rd()), distribution(min,max)
	{
		static_assert( is_arithmetic<T>::value, "RandomGenerator: template argument must be a numeric type");
		//Nothing to do
	}
	T generate()
	{
		return distribution(mt);
	}
};

