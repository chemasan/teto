/*
  Copyright (c) 2020 Jose Manuel Sanchez Madrid.
  This file is licensed under MIT license. See file LICENSE for details.
*/

#include <cpr/cpr.h>
#include <string>
#include <sstream>
#include <fstream>
#include <regex>
#include "parserequests.hpp"
#include "Request.hpp"
#include "stringfunctions.hpp"

using namespace std;
using namespace cpr;

pair<string,string> parseHeader(const string& str)
{
	auto header = split2(str,':');
	header.first = trim(header.first);
	header.second = trim(header.second);
	return header;
}
Header parseHeaders(const vector<string>& headersVector)
{
	Header ret;
	for (auto headerstr : headersVector)
	{
		auto headerPair = parseHeader(headerstr);
		ret[headerPair.first] = headerPair.second;
	}
	return ret;
}
Request parseRequest(string requestStr)
{
	stringstream source(requestStr);
	string reqLine;
	getline(source, reqLine);
	auto splittedReqLine = split2(reqLine, ' ');
	Request parsedRequest;
	parsedRequest.method = parseMethod(trim(splittedReqLine.first));
	parsedRequest.url = Url(trim(splittedReqLine.second));
	while (!source.eof())
	{
		string line;
		getline(source, line);
		if (line.empty())  break;
		auto header = parseHeader(line);
		if (!header.first.empty())  parsedRequest.headers[header.first] = header.second;
	}
	if (!source.eof())
	{
		auto offset = source.tellg();
		parsedRequest.body = Body(source.str().substr(offset));
	}
	
	return parsedRequest;
}

vector<Request> parseRequests(istream& source, const string& delimStr)
{
	vector<Request> parsedRequests;
	stringstream buffer;
	while (! source.eof())
	{
		string line;
		getline (source, line);
		if (line != delimStr)
		{
			buffer << line << endl;
		}
		if (line == delimStr || source.eof())
		{
			string tobeparsed = buffer.str();
			if (!tobeparsed.empty() && !regex_match(tobeparsed,regex("^\\s*$")) )
			{
				auto lastCharPos = (tobeparsed.size() -1);
				if (tobeparsed[lastCharPos] == '\n' && line == delimStr) tobeparsed.erase(lastCharPos);
				parsedRequests.emplace_back( parseRequest(tobeparsed) );
				buffer.str("");
			}
		}
	}
	return parsedRequests;
}
vector<Request> parseRequestsFile(const string& filePath, const string& delimiter)
{
	ifstream file(filePath);
	return parseRequests(file, delimiter);
}
void completeUrls(vector<Request>& requests, const Url& url)
{
	using namespace std::regex_constants;
	static const regex URLRE("^https?://[^\\/\\s]+/\\S*", ECMAScript | icase | optimize);
	for (Request&  request : requests)
	{
		if (!regex_match(request.url, URLRE))  request.url = url + request.url;
	}
}
void setKeepAlive(vector<Request>& requests, bool isKeepAlive)
{
	for (Request& request : requests)
	{
		if (isKeepAlive) request.headers["Connection"] = "keep-alive";
		else request.headers["Connection"] = "close";
	}
}

