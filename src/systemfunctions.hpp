/*
  Copyright (c) 2020 Jose Manuel Sanchez Madrid.
  This file is licensed under MIT license. See file LICENSE for details.
*/

#ifndef _TETO_SYSTEMFUNCTIONS_HPP
#define _TETO_SYSTEMFUNCTIONS_HPP 1

#include <string>

std::string hostname();
int pid();

#endif
