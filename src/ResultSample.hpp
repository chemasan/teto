/*
  Copyright (c) 2020 Jose Manuel Sanchez Madrid.
  This file is licensed under MIT license. See file LICENSE for details.
*/

#ifndef _TETO_RESULTSAMPLE_HPP
#define _TETO_RESULTSAMPLE_HPP 1

#include<chrono>

using namespace std;

class ResultSample
{
public:
	// Time at the request beggining acording to the system time. Use this to place the sample in a time series.
	chrono::system_clock::time_point sampleTime; 
	// Used to calculate the latency. Don't use the steady_clock time points for other things.
	chrono::steady_clock::time_point startTime; 
	chrono::steady_clock::time_point endTime;
	unsigned long long latencyNanos() const
	{
		auto duration = endTime - startTime;
		return chrono::duration_cast<chrono::nanoseconds>(duration).count();
	}
	float latencyMillis() const
	{
		return static_cast<double>(latencyNanos()) / 1000000;
	}
	unsigned int responseCode = 0;
	unsigned int responseSize = 0;
};


#endif
