/*
  Copyright (c) 2020 Jose Manuel Sanchez Madrid.
  This file is licensed under MIT license. See file LICENSE for details.
*/

#ifndef _TETO_STRINGFUNCTIONS_HPP
#define _TETO_STRINGFUNCTIONS_HPP 1

#include <string>

using namespace std;

string toLower(string str);
string ltrim(const string& orig, const string& trimchars=" \n\r\t\v"s);
string rtrim(const string& orig, const string& trimchars=" \n\r\t\v"s);
string trim(const  string& orig, const string& trimchars=" \n\r\t\v"s);
pair<string,string> split2(const string& str, char delimiter);

#endif
