/*
  Copyright (c) 2020 Jose Manuel Sanchez Madrid.
  This file is licensed under MIT license. See file LICENSE for details.
*/

#include <vector>
#include "Request.hpp"
#include "Configuration.hpp"
#include "parserequests.hpp"
#include "stressfunctions.hpp"
#include "resultsfunctions.hpp"

using namespace std;

int main(int argc, char** argv)
{
	auto config = parseConfig(argc, argv);
	Request request = { config.method, config.url, config.headers, config.data };
	vector<Request> requests;
	if (config.requestsFile.empty())   requests.emplace_back(request);
	else
	{
		requests = parseRequestsFile(config.requestsFile, config.delimiter);
		completeUrls(requests, config.url);
	}
	setKeepAlive(requests, config.connections > 0);

	if (config.warmupDurationInSecconds > 0)   runTest(requests, config.warmupDurationInSecconds, config.throwput, config.connections, config.random, config.timeoutInMillis);
	auto results = runTest(requests, config.testDurationInSecconds, config.throwput, config.connections, config.random, config.timeoutInMillis);

	displaySummary(results);
	if (!config.outputSamplesFile.empty())   saveSamples(config.outputSamplesFile, results);
	if (!config.influxUrl.empty()) saveToInflux(results, config.influxUrl, config.influxDatabase, config.influxMeasure, config.influxUser, config.influxPass);
	return 0;
}
