#!/usr/bin/env python

from flask import Flask
from flask import request

app = Flask(__name__)

@app.route("/", methods=["GET","POST","PUT"])
def hello():
	print "{} {}".format(request.method,request.path)
	print request.headers
	print
	print request.get_data()
	return "hello"

app.run()
