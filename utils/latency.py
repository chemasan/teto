#!/usr/bin/env python

from flask import Flask
from flask import request
from random import randrange
from time import sleep

app = Flask(__name__)

@app.route("/", methods=["GET","POST","PUT"])
def hello():
	latency = randrange(0,5000,100);
	print latency
	sleep(latency/1000);
	return "hello"

app.run()
