/*
  Copyright (c) 2020 Jose Manuel Sanchez Madrid.
  This file is licensed under MIT license. See file LICENSE for details.
*/


#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include "../src/parserequests.hpp"

using namespace std;

TEST_CASE("Given a request represented as text without body, parseRequest() returns the parsed request")
{
	const string original = "GET http://example.domain.tld/\nheader1: valueh1\nheader2: valueh2";
	Request expected;
	expected.method = HttpMethod::GET;
	expected.url = Url("http://example.domain.tld/");
	expected.headers["header1"] = "valueh1";
	expected.headers["header2"] = "valueh2";

	Request result = parseRequest(original);

	CHECK( result == expected );
}
TEST_CASE("Given a request represented as text with body, parseRequest() returns the parsed request")
{
	const string original = "POST http://example.domain.tld/\nheader1: valueh1\nheader2: valueh2\n\nBody of the request\nline2\nline 3";
	Request expected;
	expected.method = HttpMethod::POST;
	expected.url = Url("http://example.domain.tld/");
	expected.headers["header1"] = "valueh1";
	expected.headers["header2"] = "valueh2";
	expected.body = Body("Body of the request\nline2\nline 3");

	Request result = parseRequest(original);

	CHECK( result == expected );
}
TEST_CASE("Given a request represented as text with body and without headers, parseRequest() returns the parsed request")
{
	const string original = "POST http://example.domain.tld/\n\nBody of the request\nline2\nline 3";
	Request expected;
	expected.method = HttpMethod::POST;
	expected.url = Url("http://example.domain.tld/");
	expected.body = Body("Body of the request\nline2\nline 3");

	Request result = parseRequest(original);

	CHECK( result == expected );
}
TEST_CASE("Given a request represented as text without body and without headers, parseRequest() returns the parsed request")
{
	const string original = "GET http://example.domain.tld/";
	Request expected;
	expected.method = HttpMethod::GET;
	expected.url = Url("http://example.domain.tld/");
	
	Request result = parseRequest(original);

	CHECK( result == expected );
}

TEST_CASE("Given a istream with requests represented as text and separated with a line with the delimiter character, parseRequests() returns the parsed requests")
{
	stringstream original("GET http://example.domain.tld/\n.\nPOST http://example.domain.tld/\n\nblah\n.\nHEAD http://example.domain.tld/\n.");
	vector<Request> expected;
	Request expectedRequest1;
	expectedRequest1.method = HttpMethod::GET;
	expectedRequest1.url = "http://example.domain.tld/";
	expected.emplace_back(expectedRequest1);
	Request expectedRequest2;
	expectedRequest2.method = HttpMethod::POST;
	expectedRequest2.url = "http://example.domain.tld/";
	expectedRequest2.body = Body("blah");
	expected.emplace_back(expectedRequest2);
	Request expectedRequest3;
	expectedRequest3.method = HttpMethod::HEAD;
	expectedRequest3.url = "http://example.domain.tld/";
	expected.emplace_back(expectedRequest3);

	vector<Request> result = parseRequests(original);

	CHECK( result == expected );
}
TEST_CASE("Given a istream with requests represented as text and separated with a line with the delimiter string, parseRequests() returns the parsed requests")
{
	stringstream original("GET http://example.domain.tld/\nend\nPOST http://example.domain.tld/\n\nblah\nend\nHEAD http://example.domain.tld/\nend");
	vector<Request> expected;
	Request expectedRequest1;
	expectedRequest1.method = HttpMethod::GET;
	expectedRequest1.url = "http://example.domain.tld/";
	expected.emplace_back(expectedRequest1);
	Request expectedRequest2;
	expectedRequest2.method = HttpMethod::POST;
	expectedRequest2.url = "http://example.domain.tld/";
	expectedRequest2.body = Body("blah");
	expected.emplace_back(expectedRequest2);
	Request expectedRequest3;
	expectedRequest3.method = HttpMethod::HEAD;
	expectedRequest3.url = "http://example.domain.tld/";
	expected.emplace_back(expectedRequest3);

	vector<Request> result = parseRequests(original,"end");

	CHECK( result == expected );
}
TEST_CASE("Given a filename with requests represented as text separated with a line with the delimiter character, parseRequestsFile() returns the parsed requests")
{
	vector<Request> expected;
	Request expectedRequest1;
	expectedRequest1.method = HttpMethod::GET;
	expectedRequest1.url = "/search?q=blah";
	expected.emplace_back(expectedRequest1);
	Request expectedRequest2;
	expectedRequest2.method = HttpMethod::PUT;
	expectedRequest2.url = "http://domain.tld/id/33";
	expectedRequest2.headers["x-myheader1"] = "value1";
	expectedRequest2.headers["x-myheader2"] = "value2";
	expectedRequest2.headers["content-type"] = "application/json";
	expectedRequest2.body = Body("{\n\t\"id\": 33,\n\t\"value\": \"blah\"\n}");
	expected.emplace_back(expectedRequest2);
	Request expectedRequest3;
	expectedRequest3.method = HttpMethod::POST;
	expectedRequest3.url = "/search";
	expectedRequest3.body = Body("{\n\t\"q\": \"blah\"\n}");
	expected.emplace_back(expectedRequest3);
	Request expectedRequest4;
	expectedRequest4.method = HttpMethod::DELETE;
	expectedRequest4.url = "http://domain.tld/id/33";
	expectedRequest4.headers["x-myheader"] = "1234";
	expected.emplace_back(expectedRequest4);

	vector<Request> result = parseRequestsFile("resources/requests4tests.txt");

	CHECK( result == expected );
}
TEST_CASE("parseRequestsFile() returns the parsed requests without crashing when the given file has a trailing delimiter at the end")
{
	vector<Request> expected;
	Request expectedRequest1;
	expectedRequest1.method = HttpMethod::GET;
	expectedRequest1.url = "/path1";
	expected.emplace_back(expectedRequest1);
	Request expectedRequest2;
	expectedRequest2.method = HttpMethod::GET;
	expectedRequest2.url = "/path2";
	expected.emplace_back(expectedRequest2);

	vector<Request> result = parseRequestsFile("resources/requestsDelimTerminated.txt");

	CHECK( result == expected );
}
TEST_CASE("Given a vector of Requests, completeUrls() prepend the given url to the url of those requests that seems to miss scheme and host")
{
	Request original1;
	original1.url = "/blah";
	Request original2;
	original2.url = "http://domain.tld/blah/blah";
	Request original3;
	original3.url = ":8443/blah";
	vector<Request> requests = { original1, original2, original3 };

	completeUrls(requests, "https://domain.tld");

	CHECK( requests[0].url == "https://domain.tld/blah");
	CHECK( requests[1].url == "http://domain.tld/blah/blah");
	CHECK( requests[2].url == "https://domain.tld:8443/blah");
}
