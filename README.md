TETO
============
Copyright (c) 2020 Jose Manuel Sanchez Madrid. This file is licensed under MIT license. See file LICENSE for details.

## Overview
Teto (from *TE*sting *TO*ol) is an HTTP load testing tool. It makes HTTP requests at a fixed rate to the given URL during a certain period of time, measuring the time it takes to respond each request and it prints a summary of the response times at the end. The objective of this is to measure the maximum throughput a given HTTP service is able to manage within an acceptable response latency.

## Requirements
In addition to GNU Make and a C++17 capable compiler, the following libraries must be installed first in order to compile teto:
- [CPR](https://github.com/whoshuu/cpr/). CPR itself requires the following libraries:
	- [libcurl](https://github.com/curl/curl/)
	- [OpenSSL](https://github.com/openssl/openssl)
	- [zlib](https://github.com/madler/zlib)
- [LibInflux](https://gitlab.com/chemasan/libinflux). LibInflux itself requires CPR as well and the following libraries:
	- [JSON for Modern C++](https://github.com/nlohmann/json/)
- [Catch2](https://github.com/catchorg/Catch2/). This is required to build and run the unit tests only.

## Install
Install the required libraries indicated in the previous section.
Then run:
```
make
sudo make install
```

## Usage
Run teto with the URL to be tested as an argument. If no other options are given, teto will make GET requests to the given URL at a rate of 1 request per second during 60 secconds and will print the summary at the end:

```
$ teto example.tld
Total requests: 60
Errored requests (connection errors and timeouts): 0
% Errored requests: 0
Unsuccessful requests (errored or responded with non 2XX status code): 0
% Unsuccessful requests: 0
Minimum latency in ms: 0.557272
25th percentile in ms: 0.60901
50th percentile in ms: 0.635166
75th percentile in ms: 0.673087
90th percentile in ms: 0.718327
99th percentile in ms: 0.792422
99.9th percentile in ms: 0.893616
Maximum latency in ms: 0.893616

```
Run teto with the --help flag to print a description of all the possible options to run teto.

## TODO
- [X] save samples to a file
- [X] show number of requests
- [X] show errored requests
- [X] show % of errored requests
- [X] show minimum and maximum
- [X] show latency percentiles
- [X] allow to set request method
- [X] allow to set request body
- [X] allow to set request headers
- [X] allow to get requests from file
- [X] complete partial urls parsed from requests file
- [X] randomize requests from file
- [X] set the number of connections to use
- [ ] load request body from file using '@filepath' a la curl
- [X] set timeout
- [ ] verbose flag
- [X] version flag
- [X] easy dependency setup
- [X] reestructure folder tree
- [X] warmup time
- [ ] plot results
	- timeseries
	- percentiles
- [X] influxdb integration
- [X] BUG: trailing delimiter at the end of the requests file causes crash
- [ ] BUG: it seems the test ends before time?
- [X] Review duplication in ResultSample class
- [X] Clarify information in ResultSample class
- [X] Delimiter character maybe should be a delimiter string
- [X] Review string functions
- [ ] Consider using optional instead nullSession in doRequest
- [X] Include response size in results
- [ ] control redirects
- [ ] progressively send metrics to influxdb during the test instead at the end
- [X] LICENCE
- [X] README
- [ ] consider possible concurrency issues with session objects?
- [X] Simplify makefile
- [X] make install and make uninstall
- [ ] Include target URL in the result samples
- [ ] Include method in the result samples
