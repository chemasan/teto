# Copyright (c) 2020 Jose Manuel Sanchez Madrid.
# This file is licensed under MIT license. See file LICENSE for details.

.DEFAULT_GOAL:=bin/teto
VERSION:=$(shell git describe --tags --always --dirty)
CXXFLAGS:=-std=c++17 -Ibuild/include -g -rdynamic  -D "VERSION=\"$(VERSION)\""
LDFLAGS:=-Lbuild/lib -lcpr -lpthread -linflux -rdynamic
PREFIX ?= /usr/local
export LD_LIBRARY_PATH=build/lib
.PHONY: clean test deps cleandeps install uninstall
vpath %.cpp src
vpath %.cpp tests
vpath %.cpp build/src/backward/1.4

clean:
	rm -f *.o bin/* obj/*

bin/teto: teto.o backward.o stringfunctions.o SessionPool.o parserequests.o HttpMethod.o Configuration.o resultsfunctions.o stressfunctions.o systemfunctions.o
	$(CXX) $(LDFLAGS) -o $@  $^

install: bin/teto
	install -m 755 -D --target-directory "$(PREFIX)/bin" bin/teto

uninstall:
	rm -f "${PREFIX}/bin/teto"

bin/unittests: unittests.o parserequests.o stringfunctions.o SessionPool.o parserequests.o HttpMethod.o Configuration.o resultsfunctions.o stressfunctions.o systemfunctions.o
	$(CXX) $(LDFLAGS) -o $@  $^

test: bin/unittests
	./bin/unittests

deps:
	deptool "https://gitlab.com/chemasan/deptool-recipes/-/raw/master/tclap-1.2.2.yaml"
	deptool "https://gitlab.com/chemasan/deptool-recipes/-/raw/master/catch-2.4.0.yaml"
	deptool "https://gitlab.com/chemasan/deptool-recipes/-/raw/master/backward-1.4.yaml"
	deptool "https://gitlab.com/chemasan/deptool-recipes/-/raw/master/cpr-1.3.0.yaml"
	deptool "https://gitlab.com/chemasan/deptool-recipes/-/raw/master/libinflux-0.1.yaml"

cleandeps:
	rm -rf ./build/
